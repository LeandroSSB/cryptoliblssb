export interface Quote {
  [mimeType: string]: {
    price: number;
    last_updated: string;
  };
}

export interface Cripto {
  [mimeType: string]: {
    id: number;
    name: string;
    symbol: string;
    slug: string;
    date_added: string;
    last_updated: string;
    quote: Quote;
  };
}

export interface Data {
  data: Cripto;
}

export interface Error {
  status: {
    timestamp: string;
    error_code: number;
    error_message: string;
    elapsed: number;
    credit_count: number;
  };
}

export const isError = (error: any): error is Error => {
  return (error as Error).status !== undefined;
};

export const isData = (error: any): error is Data => {
  return (error as Data).data !== undefined;
};
