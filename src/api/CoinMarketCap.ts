import axios, { AxiosInstance, AxiosResponse } from "axios";
import { Data } from "../types/quotes";

export default class CoinMarketCap {
  baseURL: string = "https://pro-api.coinmarketcap.com/";
  axiosInstance: AxiosInstance;
  secretAPI: string;

  constructor(secretAPI: string) {
    this.secretAPI = secretAPI;
    this.axiosInstance = axios.create({
      baseURL: this.baseURL,
      headers: {
        "X-CMC_PRO_API_KEY": secretAPI,
      },
    });
  }

  async quotes(symbol: string[]) {
    const requestURL = `v1/cryptocurrency/quotes/latest?symbol=${symbol.join(
      ","
    )}`;
    const acceptedKeys = [
      "id",
      "name",
      "symbol",
      "slug",
      "date_added",
      "last_updated",
      "quote",
    ];
    try {
      const response: AxiosResponse<any, any> = await this.axiosInstance.get(
        requestURL
      );
      const data = response.data;
      let cripto: any;
      for (cripto in data.data) {
        let array = Object.entries(data.data[cripto]);
        let filter = array.filter(([key]) => {
          if (acceptedKeys.find((ackey) => ackey == key)) {
            return true;
          } else {
            return false;
          }
        });
        data.data[cripto] = Object.fromEntries(filter);
      }
      const output = {
        data: {},
      };
      output.data = data.data;
      return output as Data;
    } catch (e) {
      if (axios.isAxiosError(e)) {
        return e.response?.data as Error;
      }
      return undefined;
    }
  }

  async conversion(symbol: string, amount: number, convert: string[]) {
    const requestURL = `v1/tools/price-conversion?amount=${amount}&symbol=${symbol}&convert=${convert.join(
      ","
    )}`;
    try {
      const response: AxiosResponse<any, any> = await this.axiosInstance.get(
        requestURL
      );

      return response.data;
    } catch (e) {
      if (axios.isAxiosError(e)) {
        return e.response?.data as Error;
      }
      return undefined;
    }
  }
}
